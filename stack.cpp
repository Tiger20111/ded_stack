#include <iostream>
#include <assert.h>

#define private public // make sure we have access to private parts
#include "stack.h"
#undef private // restore privacy to private

template <typename T>
Stack<T>::Stack(int maxSize) :
    size(maxSize) // инициализация константы
{
  stack = new T[size]; // выделить память под стек
  top = 0; // number of first element in stack;
  control_sum = doStackSum(); // sum all value from each byte in stack
}

// constraction stack
template <typename T>
Stack<T>::~Stack()
{
  delete [] stack; // delete stack
}


//!
//! \tparam T       any type
//! \param value    object that we put to stack
//! \return         success or not operation
template <typename T>
inline bool Stack<T>::push(const T &value)
{
  if (!okStackSum()) {
    perror("Something was happen with stack between operations with stack");
    exit(1);
  }
  if (!okStackCanary()) {
    perror("Stack was crashed");
    exit(1);
  }
  if (top >= size) {
    perror("Stack is full");
    return false;
  }

  stack[top++] = value;

  if (!okStackCanary()) {
    perror("Stack was crashed");
    exit(1);
  }
  control_sum = doStackSum();
  return true;
}


//!
//! \tparam T   any type
//! \return     object that was taken from stack
template <typename T>
inline T Stack<T>::pop()
{
  if (!okStackSum()) {
    perror("Something was happen with stack between operations with stack");
    exit(1);
  }
  if (!okStackCanary()) {
    perror("Stack was crashed");
    exit(1);
  }

  if (top <= 0) {
    perror("Stack is empty");
    return NULL;
  }

  assert(stack[top - 1]);
  return stack[--top];
}


//!
//! \tparam T   any type
//! \return     size of stack
template <typename T>
inline int Stack<T>::getStackSize() const
{
  return size;
}


//!
//! \tparam T   any type
//! \return     show top element
template <typename T>
inline T Stack<T>::getTop() const
{
  return stack[top-1];
}

template <typename T>
void Stack<T>::showStack() const{
  for (int i = top - 1; i>=0; i--) {
    std::cout << stack[i] << " ";
  }
}

template <typename T>
bool Stack<T>::okStackCanary() const
{
  return (canary_begin == MAGIC1 && canary_end == MAGIC2);
}

template <typename T>
int Stack<T>::doStackSum() {
  char* think = (char*)this;
  int sum = 0;
  for (int i = 0; i < sizeof(this); i++) {
    sum += *think;
    think += 1;
  }
}

template <typename T>
bool Stack<T>::okStackSum() {
  int check_sum = doStackSum();
  return (check_sum == control_sum);
}