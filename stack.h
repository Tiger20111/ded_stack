#ifndef STACK_STACK_H
#define STACK_STACK_H

#define MAGIC1 20111
#define MAGIC2 797


template <typename T>
class Stack
{
 private:
  int canary_begin = MAGIC1;        // first canary
  T *stack;                         // point to stack
  const int size;                   // max size stack
  int top;                          // number of top element
  int canary_end = MAGIC2;          // second canary
  int control_sum = 0;
  bool okStackCanary() const;             // canary check
  int doStackSum(); //do control sum
  bool okStackSum(); //check control sum
 public:
  explicit Stack(int = 10);         // constructor
  ~Stack();                         // destructor

  inline bool push(const T & );     // add element to stack
  inline T pop();                   // return clone top element
  inline int getStackSize() const;  // return size of
  inline T getTop() const;          // give top element
  void showStack() const;
};

#endif //STACK_STACK_H
